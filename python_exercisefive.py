# How to get a random list from a range
import random
a = []
b = []
c = []

# Get random numbers and append to lists
for ch in range(0,20):
    n = random.randint(1,50)
    m = random.randint(1,50)
    a.append(n)
    b.append(m)
    print(a)
    print(b)


for ch in a:
    for cha in b:
        if ch == cha and ch not in c:
            c.append(ch)
            print(c)
             

#--------------------------------------------------#
#  !! i thought i got it in one line.. close-ish!!
#
# if a[ch] == b[ch] and ch not in c: 
#     c.append(ch)
#     print(c)
#--------------------------------------------------#


# ----- other attempts below ------ #

# a = [1,1,2,3,5,8,13,21,34,55,89]
# b = [1,2,3,4,5,6,7,8,9,10,11,12,13]
# c = []
# d= []
#
# for ch in a:
#     #print(ch)
#
#     for cha in b:
#         #print(cha)
#
#         if ch == cha and ch not in c:
#             c.append(ch)
#             print(c)
#            

# --- answer using dict and a new D list  ----#
#
# mylist = list(dict.fromkeys(mylist))
#  d = list(dict.fromkeys(c))
#  print(d)

# --- another attempt....   --- #
#
# element = ''
# # while element in a: 
# #     if element in b:
# #         c = c + element
# #         print(c)


# --- answer below using zip   ---- #
#
# # for a, b in zip(a,b):
# #     if a == b:
# #         c.append(a)
# #         print(c)